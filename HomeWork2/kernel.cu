#include <stdlib.h>
#include <stdio.h>
#include "cublas.h"
#include <time.h>
#define index(i,j,ld) (((j)*(ld))+(i))

void printMat(float*P, int uWP, int uHP) {
	int i, j;
	for (i = 0; i<uHP; i++) {
		printf("\n");
		for (j = 0; j < uWP; j++) {
			printf("%f ", P[index(i, j, uHP)]);
		}			
	}
}

void cpu_matrix_mult(float *h_a, float *h_b, float *h_result, int m, int n, int k) {
	for (int i = 0; i < m; ++i)
	{
		for (int j = 0; j < k; ++j)
		{
			int tmp = 0.0;
			for (int h = 0; h < n; ++h)
			{
				tmp += h_a[i * n + h] * h_b[h * k + j];
			}
			h_result[i * k + j] = tmp;
		}
	}
}

bool matrix_equal(float *h_a, float *h_b, int width, int height) {
	for (int i = 0; i < height; ++i)
	{
		for (int j = 0; j < width; ++j)
		{
			if (h_a[i * width + j] != h_b[i * width + j]) {
				return false;
			}
		}
	}
	return true;
}

int random_int(int min, int max) {
	return min + (rand() * (int)(max - min) / RAND_MAX);
}



int  main(int argc, char** argv) {
	int widthA;
	int heightA;
	int widthB;
	int heightB;
	int widthC;
	int heightC;

	printf("widthA:\n");
	scanf("%d", &widthA);
	printf("heightA:\n");
	scanf("%d", &heightA);
	printf("widthB:\n");
	scanf("%d", &widthB);
	heightB = widthA;
	widthC = widthB;
	heightC = heightA;

	cublasStatus status;

	float timerValueGPU;
	float setTimeGPU;
	cudaEvent_t start, stop;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);

	int i, j;
	cublasInit();

	float *A = (float*)malloc(heightA*widthA * sizeof(float));
	float *B = (float*)malloc(heightB*widthB * sizeof(float));
	float *C = (float*)malloc(heightC*widthC * sizeof(float));
	float *C_cpu = (float*)malloc(heightC*widthC * sizeof(float));


	for (i = 0; i < heightA; i++) {
		for (j = 0; j < widthA; j++) {
			A[index(i, j, heightA)] = (float)random_int(0, 1000);
		}
			
	}
		
	for (i = 0; i<heightB; i++)
		for (j = 0; j<widthB; j++)
			B[index(i, j, heightB)] = (float)random_int(0, 1000);;


	float* AA; float* BB; float* CC;

	cublasAlloc(heightA*widthA, sizeof(float), (void**)&AA);
	cublasAlloc(heightB*widthB, sizeof(float), (void**)&BB);
	cublasAlloc(heightC*widthC, sizeof(float), (void**)&CC);

	cudaEventRecord(start, 0);

	cublasSetMatrix(heightA, widthA, sizeof(float), A, heightA, AA, heightA);
	cublasSetMatrix(heightB, widthB, sizeof(float), B, heightB, BB, heightB);

	cudaDeviceSynchronize();
	cudaEventRecord(stop, 0);
	cudaEventSynchronize(stop);
	cudaEventElapsedTime(&setTimeGPU, start, stop);

	cudaEventRecord(start, 0);

	cublasSgemm('n', 'n', heightA, widthB, widthA, 1, AA, heightA, BB, heightB, 0, CC, heightC);

	cudaDeviceSynchronize();
	cudaEventRecord(stop, 0);
	cudaEventSynchronize(stop);
	cudaEventElapsedTime(&timerValueGPU, start, stop);

	cublasGetMatrix(heightC, widthC, sizeof(float), CC, heightC, C, heightC);


	//printf("\nMatriz A:\n");
	//printMat(A, widthA, heightA);
	//printf("\nMatriz B:\n");
	//printMat(B, widthB, heightB);
	//printf("\nMatriz C:\n");
	//printMat(C, widthC, heightC);

	clock_t startc, end;
	double cpu_time_used;

	startc = clock();

	cpu_matrix_mult(B, A, C_cpu, heightA, widthA, widthB);

	end = clock();
	cpu_time_used = ((double)(end - startc)) / CLOCKS_PER_SEC;
	cpu_time_used *= 1000;
	
	//printf("\nMatriz C_cpu:\n");
	//printMat(C_cpu, widthC, heightC);

	if (matrix_equal(C, C_cpu, widthC, heightC)) {
		printf("\n matrix are equal!");
	}
	else {
		printf("\n matrix are NOT equal!");
	}

	printf("\n cpu multiplication elapsed %f ms", cpu_time_used);
	printf("\n gpu set elapsed %f ms", setTimeGPU);
	printf("\n gpu multiplication elapsed %f ms", timerValueGPU);

	free(A);
	free(B);
	free(C);
	cublasFree(AA);
	cublasFree(BB);
	cublasFree(CC);

	status = cublasShutdown();
	getchar();
	getchar();
	return EXIT_SUCCESS;
}